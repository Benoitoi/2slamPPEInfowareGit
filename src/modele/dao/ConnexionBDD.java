package modele.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author btssio
 */
public class ConnexionBDD {

    // MYSQL
    private static String url = "jdbc:mysql://localhost:3306/InfoWare";
    private static String utilBD = "infoware_util";
    private static String mdpBD = "secret";
    
//    // ORACLE DATABASE
//    private static String url = "jdbc:oracle:thin:@localhost:1521:XE";
//    private static String utilBD = "infoware";
//    private static String mdpBD = "secret";
    
//    // JAVA DB / DERBY
//    private static String url = "jdbc:derby://localhost:1527/InfoWare";
//    private static String utilBD = "infoware_util";
//    private static String mdpBD = "secret";
    
    private static Connection cnx;

    /**
     * Retourner une connexion ; la créer si elle n'existe pas...
     *
     * @return : objet de type java.sql.Connection
     */
    public static Connection getConnexion() throws SQLException {
        if (cnx == null) {
            cnx = DriverManager.getConnection(url, utilBD, mdpBD);
            System.out.println("getConnexion : "+url);            
        }
        return cnx;
    }
    
    

}
