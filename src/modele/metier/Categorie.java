/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author btssio
 */
public class Categorie {
    private String code;
    private String libelle;
    private double salaireMini;
    private String caisseRetraite;
    private int prime;

    public Categorie(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
        this.salaireMini = 0.0;
        this.caisseRetraite = "";
        this.prime = 0;
    }
    public Categorie(String code, String libelle, double salaireMini, String caisseRetraite, int prime) {
        this.code = code;
        this.libelle = libelle;
        this.salaireMini = salaireMini;
        this.caisseRetraite = caisseRetraite;
        this.prime = prime;
    }

    public String toStringEtat() {
        return "Categorie{" + "code=" + code + ", libelle=" + libelle + ", salaireMini=" + salaireMini + ", caisseRetraite=" + caisseRetraite + ", prime=" + prime + '}';
    }
    @Override
    public String toString() {
        return libelle;
    }
    
}
