package modele.metier;

import java.util.Date;
import java.util.Locale;

//import java.sql.Date;
public class Salarie {

    private String code = "";
    private String nom = "";
    private String prenom = "";
    private Date dateNaiss;
    private Date dateEmbauche;
    private String fonction;
    private double tauxHoraire;
    private String situationFamiliale;
    private int nbrEnfants;
    private Categorie categorie;
    private Service service;


    public Salarie(String code, String nom, String prenom, Date dateNaiss, Date dateEmbauche, String fonction, double tauxHoraire, String situationFamiliale, int nbrEnfants) {
        this.code = code;
        this.nom = nom;
        this.prenom =prenom;
        this.dateNaiss = dateNaiss;
        this.dateEmbauche = dateEmbauche;
        this.fonction = fonction;
        this.tauxHoraire = tauxHoraire;
        this.situationFamiliale = situationFamiliale;
        this.nbrEnfants = nbrEnfants;
        this.categorie = null;
        this.service = null;
    }
    public Salarie(String code, String nom, String prenom, Date dateNaiss, Date dateEmbauche, String fonction, double tauxHoraire, String situationFamiliale, int nbrEnfants, Categorie cat, Service serv) {
        this( code,  nom, prenom,  dateNaiss,  dateEmbauche,  fonction,  tauxHoraire,  situationFamiliale,  nbrEnfants);
        this.categorie = cat;
        this.service = serv;
    }
    
    @Override
    public String toString() {
        String dateNaissFmt = String.format("%1$td/%1$tm/%1$tY  ", dateNaiss);
        String dateEmbFmt = String.format("%1$td/%1$tm/%1$tY  ", dateEmbauche);
        String txHoraireFmt = String.format(Locale.FRANCE, "%1$5.2f", tauxHoraire);
        String categorieToString = (categorie == null ? "Néant" : categorie.toString());
        String serviceToString = (service == null ? "Néant" : service.toString());
        return "Salarie{" + "code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaissFmt + ", dateEmbauche=" + dateEmbFmt
                + ", fonction=" + fonction + ", tauxHoraire=" + txHoraireFmt + ", situationFamiliale=" + situationFamiliale + ", nbrEnfants=" + nbrEnfants
                + ",\n categorie=" + categorieToString + ",\n service=" + serviceToString + '}';

    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(java.sql.Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public Date getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(java.sql.Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public double getTauxHoraire() {
        return tauxHoraire;
    }

    public void setTauxHoraire(double tauxHoraire) {
        this.tauxHoraire = tauxHoraire;
    }

    public String getSituationFamiliale() {
        return situationFamiliale;
    }

    public void setSituationFamiliale(String situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public int getNbrEnfants() {
        return nbrEnfants;
    }

    public void setNbrEnfants(int nbrEnfants) {
        this.nbrEnfants = nbrEnfants;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

//	@Override
//	public String toString() {
//            String msg = "";
//            msg += String.format(   "%1$-5s  %2$-12s  %3$-12s  %4$-10s  %5$-10s  %6$-23s  %7$-5s  %8$-12s  %9$-2s  %10$-12s  %11$-2s  %12$-12s  "
//                                ,   "Code", "Nom", "Prenom", "DateNaiss", "DateEmb.", "Fonction", "TauxH.", "Sit. Fam.", "nb enf.", "n°", "Catégorie");
//		msg += String.format("%1$-3s  ",code);
//		msg += String.format("%1$-12s  ",nom);
//		msg += String.format("%1$-12s  ",prenom);
//		msg += String.format("%1$td/%1$tm/%1$tY  ",dateNaiss);
//		msg += String.format("%1$td/%1$tm/%1$tY  ",dateEmbauche);
//		msg += String.format("%1$-23s  ",fonction);
//		msg += String.format(Locale.FRANCE, "%1$5.2f",tauxHoraire);
//		msg += String.format("%1$-12s  ",situationFamiliale);
//		msg += String.format("%1$-8s  ",nbrEnfants);
//		msg += String.format("%1$-3s  ",categorie.getCode());
//		msg += String.format("%1$-3s  ",categorie.getLibelle());
//		msg += String.format("%1$-3s  ",service.getCode());
//		msg += String.format("%1$-3s  ",service.getDesignation());
//		return msg;
//
//	}

}
