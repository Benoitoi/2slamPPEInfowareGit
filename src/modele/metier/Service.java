/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author btssio
 */
public class Service {
    private int code;
    private String designation;
    private String email;
    private String telephone;

    public Service(int code, String designation) {
        this.code = code;
        this.designation = designation;
        this.email = "";
        this.telephone = "";
    }
    public Service(int code, String designation, String email, String telephone) {
        this.code = code;
        this.designation = designation;
        this.email = email;
        this.telephone = telephone;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return designation;
    }
    
    public String toStringEtat() {
        return "Service{" + "code=" + code + ", designation=" + designation + ", email=" + email + ", telephone=" + telephone + '}';
    }
    
}
