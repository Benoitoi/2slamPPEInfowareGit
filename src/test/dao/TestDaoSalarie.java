package test.dao;

import modele.dao.ConnexionBDD;
import modele.dao.DaoSalarie;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Salarie;

/**
 *
 * @author nicolas
 */
public class TestDaoSalarie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("TEST DAO SALARIE SANS JOINTURE");
        // Test 1 getOneById
        System.out.println("\n Test 1 : DaoSalarie.getOneById");
        try {
            String idRecherche = "S06";
            Salarie sal = DaoSalarie.getOneById(idRecherche);
            if(sal != null){
                System.out.println("Salarie d'id "+idRecherche+" trouvé : \n"+sal.toString());
            }else{
                System.out.println("Salarie d'id "+idRecherche+" non trouvé : \n");
            }
            
        } catch (SQLException ex) {
            System.out.println("TestDaoSalarie - échec getOneById : " + ex.getMessage());
        }
        // Test 2 getAll
        System.out.println("\n Test 2 : DaoSalarie.getAll");
        try {
            ArrayList<Salarie> lesSalaries = DaoSalarie.getAll();
            for (Salarie sal : lesSalaries) {
                System.out.println(sal);
            }
            System.out.println(lesSalaries.size()+" salariés trouvés");
        } catch (SQLException ex) {
            System.out.println("TestDaoSalarie - échec getAll : " + ex.getMessage());
        }

        // Test 3 getAllByService
        System.out.println("\n Test 3 : DaoSalarie.getAllByService");
        try {
            int idService = 3;
            ArrayList<Salarie> lesSalaries = DaoSalarie.getAllByService(idService);
            for (Salarie sal : lesSalaries) {
                System.out.println(sal);
            }
            System.out.println(lesSalaries.size()+" salariés trouvés");
        } catch (SQLException ex) {
            System.out.println("TestDaoSalarie - échec getAllByService : " + ex.getMessage());
        }
        
        // Fermeture de la connexion
        try {
            ConnexionBDD.getConnexion().close();
            System.out.println("\nConnexion à la BDD fermée");
        } catch (SQLException ex) {
            System.out.println("TestDaoSalarie - échec de la fermeture de la connexion : " + ex.getMessage());
        }
    }

}
