/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.gui;

import gui.JFrameFicheSalarie;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.dao.DaoSalarie;
import modele.metier.Salarie;

/**
 *
 * @author btssio
 */
public class TestJFrameFicheSalarie {

    public static void main(String args[]) {
        JFrameFicheSalarie jfFicheSalarie = new JFrameFicheSalarie();

        // BOUCHON
//        Salarie unSalarie = new Salarie("S01", "Durand", "Jean", Date.valueOf("1985-01-15"), Date.valueOf("2013-02-20"), "Manager", 40.0d, "célibataire", 2);
        Salarie unSalarie;
        try {
            unSalarie = DaoSalarie.getOneById("S02");
            jfFicheSalarie.remplirFormulaire(unSalarie);
        } catch (SQLException ex) {
            Logger.getLogger(TestJFrameFicheSalarie.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Préparation à la visualisation
        jfFicheSalarie.setLocation(300, 200);
        jfFicheSalarie.modeVisualiser();
        jfFicheSalarie.setVisible(true);

    }
}
